# Configuration

Rename the provided example configuration file to `config.json` and configure the listed properties according to the
following list.

## Variables

### Global

<table>
	<thead>
		<tr>
			<td><b>Variable name</b></td>
			<td><b>Description</b></td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>etsTelemetryHost</td>
			<td>The IP address + port of the ETS/ATS telemetry server</td>
		</tr>
		<tr>
			<td>lametricLocalIp</td>
			<td>The IP address of LaMetric Time in the local network</td>
		</tr>
		<tr>
			<td>lametricApiKey</td>
			<td>The API key to be found at
				<a href="https://developer.lametric.com/user/devices">https://developer.lametric.com/user/devices</a>.
				Required for sending notifications to LaMetric Time.</td>
		</tr>
		<tr>
			<td>lametricAccessToken</td>
			<td>The access token generated for the indicator app</td>
		</tr>
		<tr>
			<td>lametricAppPackage</td>
			<td>The package name of the indicator app, found at the end of the push URL:
				<a href="">https://developer.lametric.com/api/v1/dev/widget/update/<u><b>com.lametric.xxx</b></u>/1</a>
				</td>
		</tr>
		<tr>
			<td>lametricAppVersion</td>
			<td>The version of the indicator app, found at the end of the push URL:
				<a href="">https://developer.lametric.com/api/v1/dev/widget/update/com.lametric.xxx/<u><b>1</b></u></a>
				</td>
		</tr>
	</tbody>
</table>

### Parameters

<table>
	<thead>
		<tr>
			<td><b>Variable name</b></td>
			<td><b>Description</b></td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>connectTimeoutSeconds</td>
			<td>How long the program should wait until retrying to establish a connection to the telemetry server and/or
				game in seconds</td>
		</tr>
		<tr>
			<td>fuelWarningIntervalSeconds</td>
			<td>The interval between low fuel warnings in seconds</td>
		</tr>
	</tbody>
</table>
