package xyz.dacookie.lametric

import java.util.logging.ConsoleHandler
import java.util.logging.Level
import java.util.logging.Logger

object Logger {
	private val loggers = mutableSetOf<Logger>()
	
	var defaultLevel: Level = Level.INFO
	
	fun setGlobalLevel(level: Level) {
		loggers.forEach {
			it.level = level
			it.handlers.forEach { handler -> handler.level = level }
		}
		defaultLevel = level
	}
	
	fun get(name: String, level: Level = defaultLevel): Logger {
		return Logger.getLogger(name).apply {
			this.level = level
			addHandler(ConsoleHandler().apply {
				this.level = level
				useParentHandlers = false
			})
		}.also { loggers.add(it) }
	}
}