package xyz.dacookie.lametric

import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.URL
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.*

object NetTools {
	private val logger = Logger.get("NetTools")
	
	fun disableSSLCertValidation() {
		val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
			override fun getAcceptedIssuers(): Array<X509Certificate> = emptyArray()
			override fun checkClientTrusted(certs: Array<X509Certificate?>?, authType: String?) {}
			override fun checkServerTrusted(certs: Array<X509Certificate?>?, authType: String?) {}
		})
		
		val sc = SSLContext.getInstance("SSL")
		sc.init(null, trustAllCerts, SecureRandom())
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
		
		// Create all-trusting host name verifier
		val allHostsValid = HostnameVerifier { _, _ -> true }
		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid)
	}
	
	var lastRequestUrl: String? = null
		private set
	
	@Throws(ConnectException::class)
	fun sendRequest(
		url: URL,
		method: String,
		headers: Map<String, String> = emptyMap(),
		body: String? = null,
		logOutput: Boolean = false
	): String {
		lastRequestUrl = url.toString()
		logger.fine("Sending $method to $lastRequestUrl")
		with(url.openConnection() as HttpURLConnection) {
			requestMethod = method
			headers.forEach(::addRequestProperty)
			
			body?.also {
				doOutput = true
				val output = outputStream.bufferedWriter()
				output.append(it)
				output.flush()
			}
			
			val response = inputStream.bufferedReader().readText()
			logger.fine("$responseCode: $responseMessage")
			if (logOutput) {
				logger.info(response)
			}
			return response
		}
	}
}