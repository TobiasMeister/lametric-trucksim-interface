package xyz.dacookie.lametric

import com.google.gson.Gson
import com.google.gson.JsonObject
import java.io.InputStreamReader
import java.net.ConnectException
import java.net.URL
import java.util.logging.Level
import kotlin.math.absoluteValue
import kotlin.math.roundToInt

val logger = Logger.get("Interface")

val configReader = InputStreamReader(ClassLoader.getSystemResource("config.json").openStream())
val config = Gson().fromJson(configReader, JsonObject::class.java)!!
val parameters = config.getAsJsonObject("parameters")!!

val etsTelemetryHost = config.getAsJsonPrimitive("etsTelemetryHost").asString!!
val etsTelemetryUrl = URL("http://$etsTelemetryHost/api/ets2/telemetry")

val lametricAppPackage = config.getAsJsonPrimitive("lametricAppPackage").asString!!
val lametricAppVersion = config.getAsJsonPrimitive("lametricAppVersion").asInt

val lametricAccessToken = config.getAsJsonPrimitive("lametricAccessToken").asString!!
val lametricApiKey = config.getAsJsonPrimitive("lametricApiKey").asString!!

val lametricLocalIp = config.getAsJsonPrimitive("lametricLocalIp").asString!!
val lametricLocalUrl =
	URL("https://$lametricLocalIp:4343/api/v1/dev/widget/update/$lametricAppPackage/$lametricAppVersion")
val lametricCloudUrl =
	URL("https://developer.lametric.com/api/v1/dev/widget/update/$lametricAppPackage/$lametricAppVersion")
val lametricLocalNotificationUrl =
	URL("http://$lametricLocalIp:8080/api/v2/device/notifications")

val lametricUpdateBody = """
	{
		"frames": [
			{
				"icon": "%ICON%",
				"text": "%TEXT%"
			}
		]
	}
""".trimIndent().replace('\t', ' ')

val lametricNotificationBody = """
	{
		"priority": "%PRIORITY%",
		"icon_type": "%ICON_TYPE%",
		"model": {
			"frames": [
				{
					"icon": "%ICON%",
					"text": "%TEXT%"
				}
			]
		}
	}
""".trimIndent().replace('\t', ' ')

val connectTimeout = parameters.getAsJsonPrimitive("connectTimeoutSeconds").asInt
val fuelWarningInterval = parameters.getAsJsonPrimitive("fuelWarningIntervalSeconds").asInt

fun main() {
	Logger.setGlobalLevel(Level.INFO)
	
	NetTools.disableSSLCertValidation()
	
	while (true) {
		try {
			// Retrieve game info
			val etsInfo = Gson().fromJson(
				NetTools.sendRequest(etsTelemetryUrl, "GET"),
				JsonObject::class.java
			)
			val game = etsInfo.getAsJsonObject("game")
			val truck = etsInfo.getAsJsonObject("truck")
			val offline = !game.getAsJsonPrimitive("connected").asBoolean
			val gear = truck.getAsJsonPrimitive("gear").asInt
			
			updateNeutralGearState(gear)
			
			updateDisplayText(offline, game, truck, gear)
			
			handleGameOffline(offline)
			if (offline) continue
			
			handleFuelWarning(truck)
			
		} catch (error: ConnectException) {
			handleConnectError()
		}
	}
}

var inNeutralSince: Long = 0

/**
 * Update neutral gear state 'since' -> Avoids showing neutral indicator when switching gears
 */
private fun updateNeutralGearState(gear: Int) {
	if (gear == 0 && inNeutralSince == 0L) inNeutralSince = System.currentTimeMillis() / 1000
	else if (gear != 0) inNeutralSince = 0
}

/**
 * Set LaMetric display text
 */
@Throws(ConnectException::class)
private fun updateDisplayText(offline: Boolean, game: JsonObject, truck: JsonObject, gear: Int) {
	val paused = game.getAsJsonPrimitive("paused").asBoolean
	val engineOn = truck.getAsJsonPrimitive("engineOn").asBoolean
	
	val displayText = when {
		offline -> "Offline"
		paused -> "Paused"
		!engineOn -> "Idle"
		else -> {
			val speed = truck.getAsJsonPrimitive("speed").asFloat.roundToInt().absoluteValue
			val parkingBrakeOn = truck.getAsJsonPrimitive("parkBrakeOn").asBoolean
			val cruiseControlOn = truck.getAsJsonPrimitive("cruiseControlOn").asBoolean
			
			val prefix = when {
				parkingBrakeOn -> "P"
				gear < 0 -> "R"
				inNeutralSince != 0L && System.currentTimeMillis() / 1000 - inNeutralSince > 2 -> "N"
				cruiseControlOn -> "C"
				else -> null
			}
			prefix?.plus(' ').orEmpty() + (if (speed > 1000) 0 else speed).toString()
		}
	}
	NetTools.sendRequest(
		lametricLocalUrl, "POST", mapOf(
			"X-Access-Token" to lametricAccessToken,
			"Cache-Control" to "no-cache",
			"Content-Type" to "application/json"
		), lametricUpdateBody
			.replace("%ICON%", "a9886" /* Animated needle gauge */)
			.replace("%TEXT%", displayText)
	)
}

/**
 * Wait until game is online
 */
private fun handleGameOffline(offline: Boolean) {
	if (offline) {
		logger.info("Game is offline, trying again in ${connectTimeout}s...")
		Thread.sleep(connectTimeout * 1000L)
	}
}

var lastFuelWarning: Long = 0

/**
 * Low fuel notification
 */
@Throws(ConnectException::class)
private fun handleFuelWarning(truck: JsonObject) {
	val fuel = truck.getAsJsonPrimitive("fuel").asFloat
	val fuelCapacity = truck.getAsJsonPrimitive("fuelCapacity").asFloat
	val fuelWarningFactor = truck.getAsJsonPrimitive("fuelWarningFactor").asFloat
	
	val fuelWarningLimit = fuelCapacity * fuelWarningFactor
	val fuelCriticalLimit = fuelWarningLimit / 2
	
	val notificationInterval = if (fuel <= fuelCriticalLimit) fuelWarningInterval / 2 else fuelWarningInterval
	val showNotification = lastFuelWarning + notificationInterval <= System.currentTimeMillis() / 1000
	
	if (!showNotification || fuel > fuelWarningLimit) return
	
	val notificationIcon = if (fuel <= fuelCriticalLimit) "29521" /* Red gas pump */ else "29518" /* Blue gas pump */
	NetTools.sendRequest(
		lametricLocalNotificationUrl, "POST", mapOf(
			"Authorization" to "Basic " + "dev:$lametricApiKey".encodeBase64ToString()
		), lametricNotificationBody
			.replace("%PRIORITY%", "critical")
			.replace("%ICON_TYPE%", "info")
			.replace("%ICON%", notificationIcon)
			.replace("%TEXT%", "Lo fuel")
	)
	lastFuelWarning = System.currentTimeMillis() / 1000
}

private fun handleConnectError() {
	if (NetTools.lastRequestUrl!! == etsTelemetryUrl.toString()) {
		logger.warning("Couldn't connect to ETS/ATS telemetry server, trying again in ${connectTimeout}s...")
	} else {
		logger.warning("Couldn't connect to LaMetric Time, trying again in ${connectTimeout}s...")
	}
	Thread.sleep(connectTimeout * 1000L)
}